# https://willschenk.com/articles/2019/installing_guix_on_nuc/
FROM alpine:3.12 AS alpine

FROM alpine AS bootstrap-guix-on-alpine
RUN apk add --no-cache gnupg xz findutils procps git
RUN addgroup -S guixbuild \
    && for i in `seq -w 1 10`; \
    do \
      adduser -S -G guixbuild -h /var/empty -s `which nologin` -g "Guix build user $i" guixbuilder$i; \
      addgroup guixbuilder$i kvm; \
    done
RUN wget -q -O - "https://sv.gnu.org/people/viewgpg.php?user_id=15145" | gpg --import -
# TODO: gpg --verify /tmp/guix-bootstrap.tar.xz
RUN wget -q -O /tmp/guix-bootstrap.tar.xz https://ftp.gnu.org/gnu/guix/guix-binary-1.1.0.x86_64-linux.tar.xz \
    && cat /tmp/guix-bootstrap.tar.xz | xz -d | tar x -C / \
    && rm -f /tmp/guix-bootstrap.tar.xz
RUN mkdir -p ~root/.config/guix \
    && ln -sf /var/guix/profiles/per-user/root/current-guix ~root/.config/guix/current
# Note: file exists.
RUN mkdir -p /usr/local/bin \
    && cd /usr/local/bin \
    && ln -sf /var/guix/profiles/per-user/root/current-guix/bin/guix
RUN source ~root/.config/guix/current/etc/profile \
    && guix archive --authorize < ~root/.config/guix/current/share/guix/ci.guix.gnu.org.pub
ARG BOOTSTRAP_GUIX_COMMIT_ID
RUN echo "BOOTSTRAP_GUIX_COMMIT_ID: ${BOOTSTRAP_GUIX_COMMIT_ID}" \
    && find / -type f -name guix-daemon -path "*bin/guix*" \
    && find / -type f -name guix -path "*/bin/guix" \
    && echo "Switching to Guix commit ${BOOTSTRAP_GUIX_COMMIT_ID} ..."
# The commit to Guix that updated the package definition of "guix" in gnu/packages/package-management.scm,
# and that is earlier (not equal to) the commit you actually want to use.
# Note:     && rm -rf /var/guix/substitute/cache (19 MB)
RUN ~root/.config/guix/current/bin/guix-daemon --build-users-group=guixbuild > /dev/null 2>&1 \
    & sleep 3 \
    && find / -type f -name guix-daemon -path "*bin/guix*" >A \
    && find / -type f -name guix -path "*/bin/guix" >>A \
    && guix pull --verbosity=0 --commit="${BOOTSTRAP_GUIX_COMMIT_ID}" \
    && hash guix \
    && guix package -p /var/guix/profiles/per-user/root/current-guix -u ".*" \
    && guix gc \
    && pkill guix-daemon \
    && rm -f /var/guix/daemon-socket/socket \
    && mkdir -p /usr/local/bin \
    && ln -sf /var/guix/profiles/per-user/root/current-guix/bin/guix /usr/local/bin/guix \
    && find / -type f -name guix-daemon -path "*bin/guix*" >B \
    && find / -type f -name guix -path "*/bin/guix" >>B \
    && (diff -ru A B || true) \
    && ls -ld /var/guix/profiles/per-user/root/current-guix*

FROM scratch AS guix-on-docker
COPY --from=bootstrap-guix-on-alpine /etc/guix /etc/guix
COPY --from=bootstrap-guix-on-alpine /var/guix /var/guix
COPY --from=bootstrap-guix-on-alpine /gnu /gnu
COPY --from=bootstrap-guix-on-alpine /usr/local /usr/local
COPY --from=bootstrap-guix-on-alpine /root/.config/guix /root/.config/guix
ADD set-mtimes.scm /
ADD etc/passwd /etc
ADD etc/group /etc
ADD etc/services /etc
ADD with-guix-daemon.scm /
RUN ["/usr/local/bin/guix", "repl", "/set-mtimes.scm"]
#ENTRYPOINT ["/root/.config/guix/current/bin/guix-daemon", "--build-users-group=guixbuild", "--disable-chroot"]
ENTRYPOINT ["guix", "repl", "--", "/with-guix-daemon.scm"]

#FROM guix-on-docker AS guix-on-docker-with-environment
#COPY --from=guix-on-docker / /
#COPY --from=bootstrap-guix-on-alpine /etc/passwd /etc
#COPY --from=bootstrap-guix-on-alpine /etc/group /etc
##COPY --from=alpine /etc/services /etc
# COPY acl / ; guix archive --authorize < acl
# TODO: glibc-utf8-locales ??
