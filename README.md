# guix-on-docker

This project builds Guix (the package manager only) as a Docker container and publishes it to registry.gitlab.com .

The final docker image is as minimal as possible--in order to make it possible to compose docker images.

If you want to use guix-on-docker as a gitlab test runnner, you should swap out the entry point so that it creates /tmp and starts the `guix-daemon` and provides a shell and coreutils to the gitlab test runner:

    default:
      image:
        name: registry.gitlab.com/daym/guix-on-docker:latest
        entrypoint: ["guix", "repl", "--", "/with-guix-daemon.scm"]

If you just want to use it standalone, then the following is totally fine:

    docker run -d --name guix registry.gitlab.com/daym/guix-on-docker
    docker exec guix guix pack hello
